package nl.jochembroekhoff.btr.api.dataopslag;

/**
 * BtrApi Prefroot placeholder. Kan worden gebruikt om een universele
 * rootklasse aan te duiden voor bijvoorbeeld opslag in de User Preferences waar
 * een klasse vereist wordt.
 *
 * @author Jochem Broekhoff
 */
public class Prefroot {

}
