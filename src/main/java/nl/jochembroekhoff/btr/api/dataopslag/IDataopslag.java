package nl.jochembroekhoff.btr.api.dataopslag;

/**
 *
 * @author Jochem Broekhoff
 */
public abstract class IDataopslag {

    public IDataopslag(String containerNaam) {
    }
    
    //CLEAR
    public abstract void clear();

    //GETTERS
    public abstract String get(String key);

    public abstract String get(String key, String standaard);

    public abstract int getInt(String key);

    public abstract int getInt(String key, int standaard);

    //SETTERS
    public abstract void set(String key, String waarde);

    public abstract void setInt(String key, int waarde);

}
