package nl.jochembroekhoff.btr.api.dataopslag;

/**
 * BtrCupApi Preferences interface. Wordt gebruikt als interface voor andere
 * pref-klassen, om het voor alle platoformen beschikbaar te maken. Voor desktop
 * computers kan hier bijvoorbeeld een implementatie met de User Preferences
 * geschreven worden, en voor Android bijvoorbeeld met de Private Shared
 * Preferences.
 *
 * @author Jochem Broekhoff
 */
public interface IPreferences {

    public IPreferences nieuw(String containerNaam);

    public void start(String containerNaam);

    public void clear();

    public String getData(String pad);

    public String getData(String pad, String standaardwaarde);

    public int getDataInt(String pad);

    public int getDataInt(String pad, int standaardwaarde);

    public void setData(String pad, String waarde);

    public void setDataInt(String pad, int waarde);
}
