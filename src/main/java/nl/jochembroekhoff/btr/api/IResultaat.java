package nl.jochembroekhoff.btr.api;

/**
 * Resultaat interface.
 *
 * @author Jochem Broekhoff
 * @param <A> het type resultaat
 * @since 1.0.0
 */
public interface IResultaat<A> extends java.io.Serializable {

    /**
     * Verkrijg het resultaat, voorkom foutmeldingen door eerst
     * <code>gelukt()</code> te checken.
     *
     * @return het resultaat van dit resultaatset
     */
    A getResultaat();

    /**
     * Of dit resultaat succesvol is.
     *
     * @return true als er een resultaat is opgehaald
     */
    public boolean gelukt();
}
