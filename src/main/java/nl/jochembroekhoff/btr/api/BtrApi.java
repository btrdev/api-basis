package nl.jochembroekhoff.btr.api;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import nl.jochembroekhoff.btr.api.dataopslag.IDataopslag;
import nl.jochembroekhoff.btr.api.dataopslag.IPreferences;
import nl.jochembroekhoff.btr.api.tools.GzipUtil;
import nl.jochembroekhoff.btr.api.tools.Base64;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * BtrApi hoofdklasse.
 *
 * @author Jochem Broekhoff
 */
public class BtrApi {
    
    //Datumformaat
    public static final DateTimeZone DT_ZONE_EUROPE_AMSTERDAM = DateTimeZone.forID("Europe/Amsterdam");
    public static final DateTimeFormatter DT_FORMATTER_JAAR_MAAND_DAG = DateTimeFormat
            .forPattern("d-M-y")
            .withZone(DT_ZONE_EUROPE_AMSTERDAM);
    public static final DateTimeFormatter DT_FORMATTER_MAAND_DAG_UUR_MINUUT = DateTimeFormat
            .forPattern("d-M k:mm")
            .withZone(DT_ZONE_EUROPE_AMSTERDAM);
    public static final SimpleDateFormat SDF_HTTP_LAST_MODIFIED = 
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

    //Dataopslag
    private static IPreferences PREFMGR;
    private static Class datamgr;
    private static final Map<String, IDataopslag> PREFMAP = new HashMap<>();

    //Tools
    public static final SimpleDateFormat DATUM_FORMAAT = new SimpleDateFormat("dd-MM-yyyy");
    private static final Calendar KALENDER = Calendar.getInstance();

    ////////////////////////////////////////////////////////////////////////////
    //DATAOPSLAG:
    public static void setDatamanager(Class type) {
        if (type.isAssignableFrom(IDataopslag.class)) {
            System.out.println("WEL GELDIGE DATAMGR");
        } else {
            System.out.println("GEEN GELDIGE DATAMGR");
        }
        datamgr = type;
    }

    public static IDataopslag getPrefContainer(String naam) {
        if (PREFMAP.containsKey(naam)) {
            return PREFMAP.get(naam);
        } else {
            try {
                return (IDataopslag) datamgr.asSubclass(datamgr)
                        .getConstructor(String.class)
                        .newInstance(naam);
            } catch (NullPointerException | IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException ex) { //TODO: Split exceptions?
                ex.printStackTrace();
                return null;
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    //TOOLS:
    /**
     * Verkrijg het weeknummer uit een Date object.
     *
     * @param datum het datum-object
     * @return het weeknummer
     */
    public static int getWeeknummerUitDatum(Date datum) {
        KALENDER.setTime(datum);
        return KALENDER.get(Calendar.WEEK_OF_YEAR);
    }

    /**
     * Verkrijg het weeknummer uit een String. Eerst wordt de datum omgezet naar
     * een Date-object waarna dat weer gebruikt wordt om het weeknummer te
     * verkrijgen uit getWeeknummerUitDatum(Date datum).
     *
     * @param datum de datum, formaat: dd-MM-yyyy
     * @return het weeknummer
     * @throws ParseException als het datumformaat incorrect is
     */
    public static int getWeeknummerUitDatum(String datum) throws ParseException {
        return getWeeknummerUitDatum(DATUM_FORMAAT.parse(datum));
    }

    /**
     * Tool, Verkrijg de dichtstbijzijnde match uit een gegeven List van
     * integers.
     *
     * @param zoek getal waar naar gezocht moet worden
     * @param lijst lijst met bruikbare getallen
     * @return de dichtstbijzijnde match
     */
    public static int getDichtstbijzindeInt(int zoek, List<Integer> lijst) {
        int min = Integer.MAX_VALUE;
        int dichtstb = zoek;
        for (int v : lijst) {
            final int diff = Math.abs(v - zoek);
            if (diff < min) {
                min = diff;
                dichtstb = v;
            }
        }

        return dichtstb;
    }

    /**
     * Trim een stuk HTML. Dit gaat bijvoorbeeld over breaks en overige spaties
     * aan het begin en einde van de string.
     *
     * @param in html input
     * @return getrimde html
     */
    public static String trimHtml(String in) {
        //TODO
        return in;
    }

    ////////////////////////////////////////////////////////////////////////////
    //SERIALISATIE TOOLS:
    public static byte[] serializableToBytes(Serializable in, boolean compress) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(in);
        oos.close();

        byte[] serBytes = baos.toByteArray();

        if (compress) {
            serBytes = GzipUtil.zip(serBytes);
        }
        
        return serBytes;
    }
    
    /**
     *
     * @param in object om te serialiseren
     * @param compress of de string nog gecomprimeerd moet worden
     * @return base64 string van het object (evt. gecomprimeerd)
     * @throws IOException
     */
    public static String serializableToString(Serializable in, boolean compress) throws IOException {
        return Base64.encode(serializableToBytes(in, compress));
    }

    /**
     *
     * @param in object om te serialiseren
     * @return base64 string van het object
     * @throws IOException
     */
    public static String serializableToString(Serializable in) throws IOException {
        return serializableToString(in, false);
    }
    
    public static <T extends Serializable> T bytesToSerializable(byte[] data, Class<T> type, boolean isCompressed) throws IOException, ClassNotFoundException {
        //Als de inhoud gecomprimeerd is, laat deze eerst uitpakken
        if (isCompressed) {
            data = GzipUtil.unzip(data);
        }

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();

        return type.cast(o);
    }

    /**
     *
     * @param <T> objecttype om naar te casten
     * @param in base64 input
     * @param type objectklasse om mee te casten
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static <T extends Serializable> T stringToSerializable(String in, Class<T> type) throws IOException, ClassNotFoundException {
        return stringToSerializable(in, type, false);
    }

    /**
     *
     * @param <T> objecttype om naar te casten
     * @param in base64 input
     * @param type objectklasse om mee te casten
     * @param isCompressed of de input string gecomprimeerd is met gzip
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static <T extends Serializable> T stringToSerializable(String in, Class<T> type, boolean isCompressed) throws IOException, ClassNotFoundException {
        byte[] data = Base64.decode(in);

        //Als de inhoud gecomprimeerd is, laat deze eerst uitpakken
        if (isCompressed) {
            data = GzipUtil.unzip(data);
        }

        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
        Object o = ois.readObject();

        return type.cast(o);
    }

}
