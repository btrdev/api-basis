package nl.jochembroekhoff.btr.api.roostering;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Naam container.
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class Naam {
    
    @Getter
    private final String voornaam;
    @Getter
    private final String achternaam;
}
