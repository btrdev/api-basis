package nl.jochembroekhoff.btr.api.roostering;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class UurMinuut implements java.io.Serializable {
    
    @Getter
    private final int uur;
    @Getter
    private final int minuut;

    public UurMinuut minus(UurMinuut that) {
        return UurMinuut.parseer(this.toMinuten() - that.toMinuten());
    }

    public int toMinuten() {
        return uur * 60 + minuut;
    }

    public static UurMinuut parseer(String input) {
        String[] split = input.split(":");
        return new UurMinuut(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
    }

    public static UurMinuut parseer(int minuten) {
        int uur = minuten / 60;
        int minuut = minuten % 60;
        return new UurMinuut(uur, minuut);
    }
}
