package nl.jochembroekhoff.btr.api.roostering;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author Jochem Broekhoff
 */
@AllArgsConstructor
public class LesTijd implements java.io.Serializable {

    @Getter
    private final UurMinuut start;
    @Getter
    private final UurMinuut einde;

    public int getAantalMinuten() {
        return einde.minus(start).toMinuten();
    }

    /**
     * Bijvoorbeeld: 8:40-9:25
     *
     * @return
     */
    @Override
    public String toString() {
        return start.getUur() + ":" + start.getMinuut() + "-"
                + einde.getUur() + ":" + einde.getMinuut();
    }

    public static LesTijd parseer(String input) {
        String[] split = input.split("-");

        if (split.length < 2) {
            return null;
        }

        return new LesTijd(UurMinuut.parseer(split[0]), UurMinuut.parseer(split[1]));
    }
}
