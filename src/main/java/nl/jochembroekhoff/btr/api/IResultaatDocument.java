package nl.jochembroekhoff.btr.api;

import org.jsoup.nodes.Document;

/**
 * Resultaatinterface met documentobject.
 *
 * @author Jochem Broekhoff
 * @since 1.0.0
 */
public interface IResultaatDocument extends IResultaat {

    /**
     * Stel het Document in.
     *
     * @param doc het document
     */
    void setDocument(Document doc);

    /**
     * Verkrijg het Document van het resultaat.
     *
     * @return het Document
     */
    Document getDocument();
}
