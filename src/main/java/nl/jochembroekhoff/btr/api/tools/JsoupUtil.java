/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.jochembroekhoff.btr.api.tools;

import java.text.ParseException;
import nl.jochembroekhoff.btr.api.BtrApi;
import org.joda.time.DateTime;
import org.jsoup.Connection;

/**
 *
 * @author jochem
 */
public class JsoupUtil {
    
    public static DateTime laatstBijgewerktOp(Connection.Response resp) {
        synchronized (BtrApi.SDF_HTTP_LAST_MODIFIED) {
            try {
                return new DateTime(BtrApi.SDF_HTTP_LAST_MODIFIED
                        .parse(resp.header("Last-Modified")))
                        .withZone(BtrApi.DT_ZONE_EUROPE_AMSTERDAM);
            } catch (ParseException ex) {
                return null;
            }
        }
    }
}
