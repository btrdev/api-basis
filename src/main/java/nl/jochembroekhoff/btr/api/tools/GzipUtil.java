package nl.jochembroekhoff.btr.api.tools;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * https://stackoverflow.com/questions/23606047/decompressing-to-a-byte-using-gzipinputstream
 *
 * @author Jochem Broekhoff
 */
public class GzipUtil {

    /**
     * Comprimeer een bytearray met gzip en krijg de bytes terug.
     *
     * @param bytesin input bytes
     * @return de ge-gzipte bytes
     */
    public static byte[] zip(final byte[] bytesin) {
        if ((bytesin == null) || (bytesin.length == 0)) {
            throw new IllegalArgumentException("Kan null of lege bytearray niet zippen");
        }

        try (ByteArrayOutputStream bout = new ByteArrayOutputStream();
                GZIPOutputStream gzipper = new GZIPOutputStream(bout)) {
            gzipper.write(bytesin, 0, bytesin.length);
            gzipper.close();

            return bout.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Gefaald inhoud te zippen", e);
        }
    }

    /**
     * Decomprimeer bytes met gzip. Als er getectecteerd wordt dat de bytes niet
     * gezipt zijn, worden de inputbytes simpelweg teruggegeven.
     *
     * @param compressed evt. ge-gzipte bytes
     * @return ge-unzipte bytes
     */
    public static byte[] unzip(final byte[] compressed) {
        if ((compressed == null) || (compressed.length == 0)) {
            throw new IllegalArgumentException("Kan null of geen bytes niet unzippen");
        }
        if (!isGezipt(compressed)) {
            return compressed;
        }

        try (ByteArrayInputStream bin = new ByteArrayInputStream(compressed);
                GZIPInputStream gzipper = new GZIPInputStream(bin)) {
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream out = new ByteArrayOutputStream();

            int len;
            while ((len = gzipper.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            gzipper.close();
            out.close();
            return out.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Gefaald inhoud te unzippen", e);
        }
    }

    /**
     * Checkt of de gegeven bytearray een geldige gzip input is aan de hand van
     * de gzip magic constante.
     *
     * @param compressed
     * @return of de input geldige gzip data is
     */
    public static boolean isGezipt(final byte[] compressed) {
        return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
    }
}
